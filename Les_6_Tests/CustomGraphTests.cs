using System.Text.RegularExpressions;
using Huiswerk6;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class CustomGraphTests
    {
        private static string StringWithoutSpaces(string s)
        {
            return Regex.Replace(s, @"\s+", "").Trim();
        }

        [Test]
        public void Graph_ToString_AfterUnweighted()
        {
            // Arrange
            IGraph graph = DSBuilder.CreateGraph14_1();

            const string expected = "V0(0) [ V1(2) V3(1) ] V1(1) [ V3(3) V4(10) ] V2(2) [ V0(4) V5(5) ] V3(1) [ V2(2) V5(8) V6(4) V4(2) ] V4(2) [ V6(6) ] V5(2) V6(2) [ V5(1) ]";

            // Act
            graph.Unweighted("V0");
            string actualWithoutSpaces = StringWithoutSpaces(graph.ToString());

            // Assert
            Assert.AreEqual(StringWithoutSpaces(expected), actualWithoutSpaces);


            Assert.Pass();
        }

        [Test]
        public void Graph_ToString_AfterDijkstra()
        {
            // Arrange
            IGraph graph = DSBuilder.CreateGraph14_1();

            const string expected = "V0(0) [ V1(2) V3(1) ] V1(2) [ V3(3) V4(10) ] V2(3) [ V0(4) V5(5) ] V3(1) [ V2(2) V5(8) V6(4) V4(2) ] V4(3) [ V6(6) ] V5(6) V6(5) [ V5(1) ]";

            // Act
            graph.Dijkstra("V0");
            string actualWithoutSpaces = StringWithoutSpaces(graph.ToString());

            // Assert
            Assert.AreEqual(StringWithoutSpaces(expected), actualWithoutSpaces);


            Assert.Pass();
        }

        [Test]
        public void Graph_IsConnected_ShouldBe()
        {
            // Arrange
            IGraph graph = DSBuilder.CreateGraph_Connected();

            const bool expected = true;

            // Act
            bool actual = graph.IsConnected();

            // Assert
            Assert.AreEqual(expected, actual);


            Assert.Pass();
        }

        [Test]
        public void Graph_IsConnected_ShouldNotBe()
        {
            // Arrange
            IGraph graph = DSBuilder.CreateGraph_Unconnected();

            const bool expected = false;

            // Act
            bool actual = graph.IsConnected();

            // Assert
            Assert.AreEqual(expected, actual);


            Assert.Pass();
        }
    }
}
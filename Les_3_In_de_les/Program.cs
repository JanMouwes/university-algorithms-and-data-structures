﻿using System;

namespace Les_3_In_de_les
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine(Sum(123));
            Console.WriteLine(Sum(2525));
            Console.WriteLine(Reverse("hallo"));
            Console.WriteLine(Reverse("parterretrap"));
        }

        public static uint Fibonacci(uint number)
        {
            if (number == 0 || number == 1)
            {
                return number;
            }

            return Fibonacci(number - 1) + Fibonacci(number - 2);
        }

        public static uint Sum(uint number)
        {
            if (number < 10)
            {
                return number;
            }

            uint leftover = number % 10;

            return Sum(number / 10) + leftover;
        }

        public static string Reverse(string word)
        {
            if (word.Length <= 1)
            {
                return word;
            }

            string firstChar = word.Substring(0, 1),
                   lastChar  = word.Substring(word.Length - 1, 1),
                   middle    = Reverse(word.Substring(1, Math.Max(word.Length - 2, 0)));

            return lastChar + middle + firstChar;
        }
    }
}
using System;
using System.Collections.Generic;


namespace Huiswerk4
{
    public static class PivotStrategies
    {
        public delegate int PivotStrategy(List<int> list, int startIndex, int endIndex);

        public static int FirstElement(List<int> list)  => list[0];
        public static int MiddleElement(List<int> list) => list[list.Count / 2];
        public static int RandomElement(List<int> list) => list[new Random().Next(list.Count - 1)];

        public static int MedianOfThreeElements(List<int> list, int startIndex, int endIndex)
        {
            int middleIndex = (endIndex - startIndex) / 2;

            int start  = list[startIndex];
            int middle = list[middleIndex];
            int end    = list[endIndex];

            bool IsMedian(int subject, int comparableA, int comparableB) => comparableA > subject == subject > comparableB;

            if (IsMedian(start, middle, end)) { return startIndex; }

            return IsMedian(middle, start, end) ? middleIndex : endIndex;
        }
    }

    public class QuickSort : Sorter
    {
        private readonly PivotStrategies.PivotStrategy pivotStrategy;


        public QuickSort() : this(PivotStrategies.MedianOfThreeElements) { }

        public QuickSort(PivotStrategies.PivotStrategy pivotStrategy)
        {
            this.pivotStrategy = pivotStrategy;
        }

        public override void Sort(List<int> list)
        {
            Sort(list, 0, list.Count - 1);
        }

        private void Sort(List<int> list, int startIndex, int endIndex)
        {
            //    Base case; If local list is of size 1 it's automatically sorted.
            if (endIndex - startIndex < 1) return;

            //    Get pivot
            int pivotIndex = pivotStrategy(list, startIndex, endIndex);
            int pivot      = list[pivotIndex];

            //    Set lower and higher bounds.
            int higherSearcherIndex = startIndex;
            int lowerSearcherIndex  = endIndex - 1;

            //    Put pivot at the end of the local list.
            Swap(list, pivotIndex, endIndex);

            //    Swap those items that aren't on the right side of the pivot
            while (higherSearcherIndex < lowerSearcherIndex)
            {
                while (higherSearcherIndex < endIndex - 1 && list[higherSearcherIndex] <= pivot) { higherSearcherIndex++; }

                while (lowerSearcherIndex > startIndex && list[lowerSearcherIndex] >= pivot) { lowerSearcherIndex--; }

                if (higherSearcherIndex > lowerSearcherIndex) break;

                Swap(list, higherSearcherIndex, lowerSearcherIndex);
            }

            //    Swap pivot into the middle of the local list
            Swap(list, higherSearcherIndex, endIndex);

            //    Sort lower & higher half of the list (higherSearcherIndex is the location of the pivot)
            Sort(list, startIndex, higherSearcherIndex - 1);
            Sort(list, higherSearcherIndex             + 1, endIndex);
        }

        //    Swap without local temp variable
        private static void Swap(IList<int> list, int indexA, int indexB)
        {
            if (indexA == indexB) return;

            list[indexA] += list[indexB];
            list[indexB] =  list[indexA] - list[indexB];
            list[indexA] -= list[indexB];
        }
    }
}
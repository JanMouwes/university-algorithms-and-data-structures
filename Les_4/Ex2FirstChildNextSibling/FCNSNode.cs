namespace Huiswerk4
{
    public class FCNSNode<T> : IFCNSNode<T>
    {
        private FCNSNode<T> firstChild;
        private FCNSNode<T> nextSibling;
        private T           data;

        public FCNSNode(T data, FCNSNode<T> firstChild, FCNSNode<T> nextSibling)
        {
            this.data = data;

            this.firstChild  = firstChild;
            this.nextSibling = nextSibling;
        }

        public FCNSNode(T data) : this(data, null, null) { }

        public T GetData()
        {
            return data;
        }

        public FCNSNode<T> GetFirstChild()
        {
            return firstChild;
        }

        public FCNSNode<T> GetNextSibling()
        {
            return nextSibling;
        }
    }
}
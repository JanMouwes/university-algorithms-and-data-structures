using System;
using System.Collections.Generic;
using System.Text;

namespace Huiswerk4
{
    public class FirstChildNextSibling<T> : IFirstChildNextSibling<T>
    {
        public FCNSNode<T> root;

        public int Size()
        {
            int GetSize(IFCNSNode<T> node)
            {
                if (node == null) return 0;

                return 1 + GetSize(node.GetFirstChild()) + GetSize(node.GetNextSibling());
            }

            return GetSize(root);
        }

        public void PrintPreOrder()
        {
            const int  gutterSize   = 2;
            const char gutterSymbol = ' ';

            LinkedList<(T, int)> list = new LinkedList<(T, int)>();

            int maxLevel     = 0;
            int maxDataWidth = 0;

            void TraverseNode(IFCNSNode<T> node, int level)
            {
                if (node == null) return;

                if (maxLevel < level) { maxLevel = level; }

                if (maxDataWidth < node.GetData().ToString().Length) { maxDataWidth = node.GetData().ToString().Length; }

                list.AddLast((node.GetData(), level));
                TraverseNode(node.GetFirstChild(), level + 1);
                TraverseNode(node.GetNextSibling(), level);
            }

            TraverseNode(root, 0);

            StringBuilder builder = new StringBuilder();
            foreach ((T item1, int item2) in list)
            {
                int dataWidthDeficit = maxDataWidth - item1.ToString().Length;

                builder.Append(new string(gutterSymbol, item2 * (gutterSize + maxDataWidth)));
                builder.Append(item1 + new string(gutterSymbol, dataWidthDeficit));
                builder.Append('\n');
            }

            Console.WriteLine(builder);
        }

        public override string ToString()
        {
            if (root == null) return "NIL";

            string NodeToString(IFCNSNode<T> node)
            {
                return node.GetData()
                     + (node.GetFirstChild()  != null ? ",FC(" + NodeToString(node.GetFirstChild())  + ")" : "")
                     + (node.GetNextSibling() != null ? ",NS(" + NodeToString(node.GetNextSibling()) + ")" : "");
            }

            return NodeToString(root);
        }
    }
}
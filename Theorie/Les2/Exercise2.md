## Exercise 1

Given the following sequence: add(2), add(1), remove(), add(4), add(3), remove(), remove(), add(5).

```
{2, 1, 4, 3, 5}
```

a. Show all steps of this sequence for a stack implemented as a linked list.

```

```

b. Show all steps of this sequence for a stack implemented as an array.

```
| 2 | 1 | null | null |
```

c. Show all steps of this sequence for a queue implemented as a linked list.
d. Show all steps of this sequence for a queue implemented as an array.

Assume an initial size of 4 for the array implementations.

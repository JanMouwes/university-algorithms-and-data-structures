## Exercise 1

Show the results of the following sequence: add(2), add(7), add(0), add(4), remove(), remove() when the add() and remove() operations correspond to the basic operations in the following:

a. Stack 
```
{2, 7}
```

b. Queue
```
{0, 4}
```
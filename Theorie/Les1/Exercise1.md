## Exercise 1

Group the following into Big-Oh functions:
 
`x2, x, x3 +x, x2 –x, (x4 /(x–1))`

| Formula | Big-Oh |
|---:|:---|
| `x^2` | `O(N^2)`|
| `x` | `O(N)` |
| `x^3 + x` | `O(N^3)` |
| `x^2 - x` | `O(N^2)` |
| `x^4 / (x-1)` | `O(N^3)` |

| Big-Oh | Group |
|:---| ---:|
| `O(N)` |  `x` |
| `O(N^2)`|  `x^2`,`x^2 - x` |
| `O(N^3)` |  `x^3 + x`,`x^4 / (x-1)` |


## Exercise 3

An algorithm takes 0.5 ms for input size 100. How large a problem can be solved in 1 minute (assuming that low-order terms are negligible) if the running time is

a) Linear

b) Quadratic

c) Cubic

| Big-Oh | N1 | N2 |
|---:|:---|:---|
| `O(N)` | `100` | `12,000,000` |
| `O(N^2)` | `100` | `34,641` |
| `O(N^3)` | `100` | `1,062` |

`T = (N2/N1)^X * S`

For O(N)
```
60,000 = (N2/100)^1 * 0.5

120,000 = (N2/100)^1

120,000 = N2/100

120,000 = 0.01N2

12,000,000 = N2
```

For O(N^2)
```
60,000 = (N2/100)^2 * 0.5

120,000 = (N2/100)^2

120,000 = (0.01N2)^2

√120,000 = 0.01N2

100 * √120,000 = N2

34641.016151377545871 = N2
```

For O(N^3)
```
60,000 = (N2/100)^2 * 0.5

120,000 = (N2/100)^3

120,000 = (0.01N2)^3

∛120,000 = 0.01N2

100 * ∛120,000 = N2

1062.658569182611066 = N2
```

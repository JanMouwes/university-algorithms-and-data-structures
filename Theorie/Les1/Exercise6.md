## Exercise 6

Wat is de tijdscomplexiteit van onderstaande code?
```
void main() {
   sum = 0;
   for (int i = 0; i < n; i++)
for (int j = i; j < n * n; j++) sum += functie(i, j);
}
int functie(int x, int y)
{
   int iets = 0;
   for (int k = 0; k < y; k++)
      iets += functie2(x);
   return iets;
}
int functie2(int x)
{
   int iets = 0;
   for (int k = 1; k < x; k = k * 3)
      iets++;
   return iets;
}
```

main has a time complexity of O(N^3 * N^2 log3 N) or O(N^5 log3 N)

functie has a time complexity of O(N log3 N) but is called with N^2 therefore O(N^2 log3 N)

functie2 has a time complexity of O(log3 N)
## Exercise 8

In deze opdracht bekijken we logaritmen. Steeds is het grondtal 2.

a) Bekijk nog eens de grafiek die je gemaakt hebt in Opdracht 6a. Lees de waardeafvan log 4, log 8, log 16, log 32, log 64, log 128, log 256, log 512.
`2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`

b) Wat is de waarde van log 1024? En log 2048?
`10`, `11`

c) Als geldt: `log x = 4`, wat is dan de waarde van `x`?
`16`

d) Als geldt: `log x = 10`, wat is dan de waarde van `x`?
`1024`

## Exercise 2

An algorithm takes 0.4 ms for input size 100. How long will it take for input size 500 (assuming that low-order terms are negligible) if the running time is:

a) Linear

b) Quadratic 

c) Cubic

| Big-Oh | N1: 100 | N2: 500 |
|---:|:---|:---|
| `O(N)` | `0.4ms` | `2ms` |
| `O(N^2)` | `0.4ms` | `10ms` |
| `O(N^3)` | `0.4ms` | `50ms` |

`T = (N2/N1)^X * S`

For O(N^2)

`T = (500/100)^1 * 0.4`

`T = (5)^1 * 0.4`

`T = 5 * 0.4`

`T = 2`

For O(N^2)

`T = (500/100)^2 * 0.4`

`T = (5)^2 * 0.4`

`T = 25 * 0.4`

`T = 10`

For O(N^3)

`T = (500/100)^3 * 0.4`

`T = (5)^3 * 0.4`

`T = 125 * 0.4`

`T = 50`
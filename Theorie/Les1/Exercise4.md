## Exercise 4

In terms of `N`, what is the running time of the following algorithm to compute `X^N`:

```
public static double power(double x, int n) { 
    double result = 1.0;
    
    for (int i = 0; i < n; i++)
        result *= x;
    
    return result;
}
```

### Answer:
`O(N)`
## Exercise 9

Volgens de theorie heeft een algoritme een complexiteit als de rekentijd gelijk blijkt aan bijvoorbeeld . Een kwadratische complexiteit is daarom voor grote altijd ongunstiger dan een lineaire omdat er altijd een gevonden kan worden
waarvoor de kwadratische functie groter wordt dan de lineaire. We gaan dit voor een drietal gevallen onderzoeken.

a) Bepaal door berekening of door het tekenen van de grafieken voor welke `x` de grafiek van `x^2` groter wordt dan `10x`. Bepaal ook de `x` waarvoor `x^2` groter wordt dan  `1000x`. Denk je dat er een (eindige) waarde `a` te vinden is waarvoor de grafiek van `ax` altijd groter zal blijven dan `x^2`?

b) Bepaal door de grafieken te bestuderen voor welke `x` de grafiek van `x^2` groter is dan die van `1000x`. Wat zegt dit over binair zoeken (in een gesorteerde lijst) in vergelijking met lineair zoeken?

c) Bepaal op dezelfde wijze voor welke de grafiek van groter is dan die van . We zullen in het college over sorteren ontdekken wat het belang

is van dit resultaat!

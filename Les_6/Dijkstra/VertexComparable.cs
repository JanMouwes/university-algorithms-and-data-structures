using System;
using System.Reflection.Metadata.Ecma335;

namespace Huiswerk6.Dijkstra
{
    public struct VertexComparable : IComparable<VertexComparable>
    {
        public Vertex Vertex { get; set; }

        public double Dist { get; set; }

        public Vertex Prev { get; set; }

        public VertexComparable(Vertex vertex, Vertex prev, double dist)
        {
            this.Vertex = vertex;
            this.Dist = dist;
            this.Prev = prev;
        }

        public int CompareTo(VertexComparable other) => other.Dist > this.Dist ? -1 : other.Dist < this.Dist ? 1 : 0;
    }
}
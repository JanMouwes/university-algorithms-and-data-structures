using System;
using System.Diagnostics;
using Les_1;

namespace Algoritmiek_Datastructuur_Huiswerk.Homework.Les1
{
    public class Exercise5 : IExercise
    {
        public void Run()
        {
            int[] values = new[] {1, 100, 10000, 1000000};

            foreach (int value in values)
            {
                Console.WriteLine("Elapsed for N = " + value);
                Run(value);
            }
        }

        public int WeekNumber     => 1;
        public int ExerciseNumber => 5;

        private void Run(int n)
        {
            int sum = 0;

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();
            // Fragment 1
            for (int i = 0; i < n; i++)
                sum++;
            stopwatch.Stop();
            Console.WriteLine($"Fragment 1: {stopwatch.Elapsed}");

            stopwatch.Start();
            // Fragment 2
            for (int i = 0; i < n; i += 2)
                sum++;
            stopwatch.Stop();
            Console.WriteLine($"Fragment 2: {stopwatch.Elapsed}");

            stopwatch.Start();
            // Fragment 3
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    sum++;

            stopwatch.Stop();
            Console.WriteLine($"Fragment 3: {stopwatch.Elapsed}");

            stopwatch.Start();
            // Fragment 4
            for (int i = 0; i < n; i++)
                sum++;
            for (int j = 0; j < n; j++)
                sum++;

            stopwatch.Stop();
            Console.WriteLine($"Fragment 4: {stopwatch.Elapsed}");

            stopwatch.Start();
            // Fragment 5
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n * n; j++)
                    sum++;

            stopwatch.Stop();
            Console.WriteLine($"Fragment 5: {stopwatch.Elapsed}");

            stopwatch.Start();
            // Fragment 6
            for (int i = 0; i < n; i++)
                for (int j = 0; j < i; j++)
                    sum++;

            stopwatch.Stop();
            Console.WriteLine($"Fragment 6: {stopwatch.Elapsed}");

            stopwatch.Start();
            // Fragment 7
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n * n; j++)
                    for (int k = 0; k < j; k++)
                        sum++;

            stopwatch.Stop();
            Console.WriteLine($"Fragment 7: {stopwatch.Elapsed}");

            stopwatch.Start();
            // Fragment 8
            for (int i = 1; i < n; i = i * 2)
                sum++;
            stopwatch.Stop();
            Console.WriteLine($"Fragment 8: {stopwatch.Elapsed}");
        }
    }
}
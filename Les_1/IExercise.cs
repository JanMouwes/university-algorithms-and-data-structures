namespace Les_1
{
    public interface IExercise
    {
        void Run();

        int WeekNumber { get; }
        int ExerciseNumber { get; }
    }
}
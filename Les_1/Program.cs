﻿using System;
using Les_1.SieveOfErastosthenes;

namespace Les_1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            IExercise[] exercises =
            {
//                new Exercise5(),
                new Praktijk()
            };

            foreach (IExercise exercise in exercises)
            {
                Console.WriteLine($"Week {exercise.WeekNumber}: ");

                exercise.Run();

                Console.WriteLine($"End week {exercise.WeekNumber}");
            }
        }
    }
}
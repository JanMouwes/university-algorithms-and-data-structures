using System.Collections.Generic;
using System.Linq;

namespace Les_1.SieveOfErastosthenes
{
    public class Sieve : ISieve
    {
        /// <summary>
        /// Determines primes for a given range
        /// </summary>
        /// <param name="inputNumbers">Range of numbers to be given</param>
        /// <param name="knownPrimes">Known primes until the start of the number-range</param>
        /// <returns>Primes</returns>
        public List<int> DeterminePrimes(Queue<int> inputNumbers, LinkedList<int> knownPrimes)
        {
            bool IsPrime(int number) => number > 1 && knownPrimes.All(prime => number % prime != 0);

            while (inputNumbers.Count > 0)
            {
                int currentNumber = inputNumbers.Dequeue();

                if (!IsPrime(currentNumber)) continue;

                knownPrimes.AddLast(currentNumber);
            }

            return new List<int>(knownPrimes);
        }

        public List<int> DeterminePrimes(Queue<int> inputNumbers) => this.DeterminePrimes(inputNumbers, new LinkedList<int>());

        public List<int> DeterminePrimes(List<int> inputNumbers) => this.DeterminePrimes(new Queue<int>(inputNumbers));
    }
}
using System;
using System.Collections.Generic;
using System.Linq;

namespace Les_1.SieveOfErastosthenes
{
    public class Praktijk : IExercise
    {
        public void Run()
        {
            Queue<int> range = new Queue<int>(Enumerable.Range(0, 121));

            Sieve     sieve  = new Sieve();
            List<int> primes = sieve.DeterminePrimes(range);

            Console.WriteLine("I found these primes: " + string.Join(", ", primes));
        }

        public int WeekNumber     => 1;
        public int ExerciseNumber => 10;
    }
}
using System.Collections.Generic;

namespace Les_1.SieveOfErastosthenes
{
    public interface ISieve
    {
        List<int> DeterminePrimes(List<int> inputNumbers);
    }
}
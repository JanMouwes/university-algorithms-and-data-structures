﻿using System;

namespace Huiswerk3
{
    public class Opgave2
    {
        static long calls = 0;

        public static long FibonacciRecursive(int n)
        {
            calls++;

            if (n == 0 || n == 1) return n;

            return FibonacciRecursive(n - 1) + FibonacciRecursive(n - 2);
        }

        public static long FibonacciIterative(int n)
        {
            calls++;

            long previousValue = 0;
            long currentValue  = 1;
            for (int i = 0; i < n; i++)
            {
                long oldCurrentValue = currentValue;
                currentValue  += previousValue;
                previousValue =  oldCurrentValue;
            }

            return previousValue;
        }

        public static void Run()
        {
            int MAX = 35;

            calls = 0;
            System.Console.WriteLine("Recursief:");
            for (int n = 1; n <= MAX; n++)
            {
                System.Console.WriteLine("          Fibonacci({0,2}) = {1,8} ({2,9} calls)", n, FibonacciRecursive(n), calls);
            }

            calls = 0;
            System.Console.WriteLine("Iteratief:");
            for (int n = 1; n <= MAX; n++)
            {
                System.Console.WriteLine("          Fibonacci({0,2}) = {1,8} ({2,9} loops)", n, FibonacciIterative(n), calls);
            }
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;

namespace Huiswerk3
{
    public class Opgave6
    {
        public static string ForwardStringJoin(List<int> list, int from)
        {
            if (from <= 0 || list.Count == 0) return string.Join(" ", list);

            list.RemoveAt(0);
            return ForwardStringJoin(list, from - 1);
        }

        public static string ForwardString(List<int> list, int from)
        {
            if (list.Count <= 1) return from <= 0 && list.Count > 0 ? list[0].ToString() : string.Empty;

            string returnString = from <= 0 ? list[0] + " " : "";

            list.RemoveAt(0);

            return returnString + ForwardString(list, from - 1);
        }

        public static string BackwardString(List<int> list, int to)
        {
            return string.Join(" ", ForwardStringJoin(list, to).Split(" ").Reverse());
        }

        public static void Run()
        {
            List<int> list = new List<int>(new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});

            System.Console.WriteLine(ForwardString(list, 3));
            System.Console.WriteLine(ForwardString(list, 7));
            System.Console.WriteLine(BackwardString(list, 3));
            System.Console.WriteLine(BackwardString(list, 7));
        }
    }
}
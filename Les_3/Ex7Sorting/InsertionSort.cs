using System;
using System.Collections.Generic;
using System.Linq;


namespace Huiswerk3
{
    public class InsertionSort : Sorter
    {
        public override void Sort(List<int> list)
        {
            LinkedList<int> sortedList = new LinkedList<int>();

            list.ForEach(item =>
            {
                LinkedListNode<int> currentNode = sortedList.First;

                if (currentNode == null)
                {
                    sortedList.AddFirst(item);
                    return;
                }

                while (currentNode.Value <= item)
                {
                    if (currentNode.Next == null)
                    {
                        sortedList.AddAfter(currentNode, item);
                        return;
                    }

                    currentNode = currentNode.Next;
                }

                sortedList.AddBefore(currentNode, item);
            });

            list.Clear();
            list.AddRange(sortedList);
        }
    }
}
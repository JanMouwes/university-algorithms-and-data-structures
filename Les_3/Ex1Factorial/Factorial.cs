﻿using System;
using System.Diagnostics;

namespace Huiswerk3
{
    public class Opgave1
    {
        /*
         * The recursive implementation is slightly faster than the non-recursive implementation 
         */

        public static long FacRecursive(int n)
        {
            if (n == 1) return n;

            return n * FacRecursive(n - 1);
        }

        public static long FacIterative(int n)
        {
            long returnValue = 1;
            for (; n != 0; n--)
            {
                returnValue *= n;
            }

            return returnValue;
        }

        public static void Run()
        {
            //------------------------------------------------------------
            // The factorial of 21 is too high to fit in a "long". That's
            // why from n=21, the result is negative
            //------------------------------------------------------------
            const int MAX       = 22;
            Stopwatch stopwatch = Stopwatch.StartNew();

            Console.WriteLine("Iteratief:");
            for (int i = 0; i < 100000; i++)
            {
                for (int n = 1; n < MAX; n++)
                {
                    FacRecursive(n);
                    continue;

                    Console.WriteLine("          {0,2}! = {1,20}", n, FacIterative(n));
                }
            }

            Console.WriteLine($"Elapsed for iterative: {stopwatch.Elapsed}");
            stopwatch.Restart();

            Console.WriteLine("Recursief:");
            for (int i = 0; i < 100000; i++)
            {
                for (int n = 1; n < MAX; n++)
                {
                    FacRecursive(n);
                    continue;

                    Console.WriteLine("          {0,2}! = {1,20}", n, FacRecursive(n));
                }
            }

            Console.WriteLine($"Elapsed for recursive: {stopwatch.Elapsed}");
        }
    }
}
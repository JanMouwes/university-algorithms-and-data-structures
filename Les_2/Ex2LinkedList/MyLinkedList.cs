﻿using System.Text;

namespace Huiswerk2
{
    public class MyLinkedList<T> : IMyLinkedList<T>
    {
        public  MyNode<T> header;
        private int       size;

        public MyLinkedList()
        {
            header = null;
            size   = 0;
        }

        //    O(1)
        public void AddFirst(T data)
        {
            Insert(0, data);
        }

        //    O(1)
        public T GetFirst()
        {
            if (header == null)
            {
                throw new MyLinkedListEmptyException();
            }

            return header.data;
        }

        //    O(1)
        public void RemoveFirst()
        {
            if (header == null)
            {
                throw new MyLinkedListEmptyException();
            }

            header = header.next;
            size--;
        }

        //    O(1)
        public int Size()
        {
            return size;
        }

        //    O(1)
        public void Clear()
        {
            header = null;
            size   = 0;
        }

        //    O(N)
        public void Insert(int index, T data)
        {
            if (index > size || index < 0)
            {
                throw new MyLinkedListIndexOutOfRangeException();
            }

            MyNode<T> prevNode = header;

            //    Move to the 'previous' node, so index - 1
            int currentIndex = 0;
            while (currentIndex < index - 1)
            {
                prevNode = prevNode.next;

                currentIndex++;
            }

            //    Determine 'next' node, either the previous node's next node or the header.
            MyNode<T> nextNode = index != 0 ? prevNode.next : header;
            MyNode<T> newNode  = new MyNode<T> {data = data, next = nextNode};

            //    Increment size
            size++;
            
            //    Place node back onto the linked list
            if (nextNode == header)
            {
                header = newNode;
                return;
            }

            prevNode.next = newNode;
        }

        public override string ToString()
        {
            if (header == null)
            {
                return "NIL";
            }

            StringBuilder builder     = new StringBuilder("[");
            MyNode<T>     currentNode = header;

            builder.Append(currentNode.data);
            while (currentNode.next != null)
            {
                currentNode = currentNode.next;
                builder.Append(",");
                builder.Append(currentNode.data);
            }

            builder.Append("]");

            return builder.ToString();
        }
    }
}
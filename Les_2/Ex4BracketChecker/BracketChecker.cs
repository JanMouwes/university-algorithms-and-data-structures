using System;
using System.Collections.Generic;

namespace Huiswerk2
{
    public static class BracketChecker
    {
        public static bool CheckBrackets(string s)
        {
            Dictionary<char, char> oppositesDictionary = new Dictionary<char, char>
            {
                {'(', ')'}
            };
            return CheckBrackets(s, oppositesDictionary);
        }

        public static bool CheckBrackets2(string s)
        {
            Dictionary<char, char> oppositesDictionary = new Dictionary<char, char>
            {
                {'(', ')'},
                {'[', ']'},
                {'{', '}'},
            };

            return CheckBrackets(s, oppositesDictionary);
        }

        private static bool CheckBrackets(string s, IReadOnlyDictionary<char, char> oppositesDictionary)
        {
            char[] chars = s.ToCharArray();

            IMyStack<char> charStack = new MyStack<char>();


            foreach (char c in chars)
            {
                if (oppositesDictionary.ContainsKey(c))
                {
                    charStack.Push(c);
                }
                else if (charStack.IsEmpty())
                {
                    return false;
                }
                else if (oppositesDictionary[charStack.Top()] == c)
                {
                    charStack.Pop();
                }
            }

            return charStack.IsEmpty();
        }
    }

    class BracketCheckerInvalidInputException : Exception
    {
    }
}
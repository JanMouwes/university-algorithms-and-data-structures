namespace Huiswerk2
{
    public class MyStack<T> : IMyStack<T>
    {
        private readonly IMyLinkedList<T> linkedList = new MyLinkedList<T>();

        public bool IsEmpty()
        {
            return linkedList.Size() == 0;
        }

        public T Pop()
        {
            T first = Top();
            linkedList.RemoveFirst();

            return first;
        }

        public void Push(T data)
        {
            linkedList.AddFirst(data);
        }

        public T Top()
        {
            if (IsEmpty())
            {
                throw new MyStackEmptyException();
            }

            return linkedList.GetFirst();
        }
    }
}
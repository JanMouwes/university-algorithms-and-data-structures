﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Huiswerk2
{
    public class MyArrayList : IMyArrayList
    {
        private int[] data;
        private int   size = 0;

        // Big-Oh: O(1)
        public MyArrayList(int capacity)
        {
            data = new int[capacity];
            size = 0;
        }

        // Big-Oh: O(1)
        public void Add(int n)
        {
            if (size == data.Length)
            {
                throw new MyArrayListFullException();
            }
            Set(size++, n);
        }

        // Big-Oh: O(1)
        public int Get(int index)
        {
            if (index >= size || index < 0)
            {
                throw new MyArrayListIndexOutOfRangeException();
            }

            return data[index];
        }

        // Big-Oh: O(1)
        public void Set(int index, int n)
        {
            if (index >= size || index < 0)
            {
                throw new MyArrayListIndexOutOfRangeException();
            }

            data[index] = n;
        }

        // Big-Oh: O(1)
        public int Capacity()
        {
            return data.Length;
        }

        // Big-Oh: O(1)
        public int Size()
        {
            return size;
        }

        // Big-Oh: O(1)
        public void Clear()
        {
            size = 0;
        }

        // Big-Oh: O(N)
        public int CountOccurences(int n)
        {
            int count = 0;
            for (int i = 0; i < size; i++)
            {
                if (data[i] == n) count++;
            }

            return count;
        }

        // Big-Oh: O(N)
        public override string ToString()
        {
            if (size == 0)
            {
                return "NIL";
            }

            StringBuilder builder = new StringBuilder("[");
            int[] dataSubArray = new int[size];
            for (int i = 0; i < size; i++)
            {
                dataSubArray[i] = data[i];
            }
            
            builder.Append(string.Join(",", dataSubArray));
            builder.Append("]");
            return builder.ToString();
        }
    }
}
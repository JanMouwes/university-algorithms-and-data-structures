using System;
using System.Collections.Generic;

namespace Huiswerk2
{
    public class MyQueue<T> : IMyQueue<T>
    {
        private readonly IList<T> list = new List<T>();

        public bool IsEmpty()
        {
            return list.Count == 0;
        }

        public void Enqueue(T data)
        {
            list.Insert(list.Count, data);
        }

        public T GetFront()
        {
            if (IsEmpty())
            {
                throw new MyQueueEmptyException();
            }

            return list[0];
        }

        public T Dequeue()
        {
            T item = GetFront();
            list.RemoveAt(0);
            return item;
        }

        public void Clear()
        {
            list.Clear();
        }
    }
}